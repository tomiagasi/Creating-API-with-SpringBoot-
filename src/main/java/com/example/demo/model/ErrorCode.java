package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorCode {

    private String code;

    private String message;

}
