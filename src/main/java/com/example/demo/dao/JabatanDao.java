package com.example.demo.dao;

import com.example.demo.model.Jabatan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JabatanDao extends JpaRepository<Jabatan, Integer> {
}
